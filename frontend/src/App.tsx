import React, { Component } from 'react';
import './App.css';

import {observable, computed, toJS} from 'mobx';
import {observer} from 'mobx-react';
import GoogleMapReact, { Coords } from 'google-map-react';

//const host = 'https://api2.coronadar.de/location';
const host = 'http://localhost:6066/location';

interface AppProps{
  
}


interface WeightedLocation{
  location: google.maps.LatLng;
  weight: number;
}



@observer
export class App extends Component<AppProps>
{


  @observable
  _lastPos: Position | undefined;

  @computed
  get mapCenter() : Coords
  {
    if(this._lastPos){
      const {latitude: lat, longitude: lng} = this._lastPos.coords;
      return{lat, lng};
    }

    return {lat: 52.51806822896771, lng: 13.376367009578644};
  }  

  @observable
  mapZoom : number = 10;


  @observable 
  heatmapLayer : any = "";

  @observable
  headmapPoints : google.maps.LatLng[] = [];

  _heatmapLayer: google.maps.visualization.HeatmapLayer | undefined;
  _fetchLoopShouldRun: boolean = false;
  _underlyingMap: google.maps.Map<HTMLDivElement> | undefined;
  

  _lastPositionId : string|undefined;

  
  
  private handleApiLoaded( map: google.maps.Map<HTMLDivElement>, maps: any, ref: Element | null) { 

    this._underlyingMap = map;
/**
 * {
      map: map,
      data: []
    }
 */
    this._heatmapLayer = new google.maps.visualization.HeatmapLayer();
    //this._heatmapLayer.set('dissipating', false);
    this._heatmapLayer.setMap(map);
    this._heatmapLayer.setData([]);

    this._startFetchLoop();
  }


  private async _wait(millis : number){
    return new Promise((res) => setTimeout(res.bind(this), millis));
  }

  private async _startFetchLoop(){
    this._fetchLoopShouldRun = true;

    while(this._fetchLoopShouldRun){
      await this._fetchData();
      await this._wait(2000);
    }    
  }


  private _buildHttpQueryPart(map : {[key: string] : any}, parent? :string) : string{
    return Object.keys(map).map(x => {

      let key = x;
      if(parent){
        key = `${parent}[${x}]`;
      }

      if(typeof map[x] === "object"){
        return this._buildHttpQueryPart(map[x], x);        
      }

      const string = `${encodeURIComponent(key)}=${map[x]}`;
      return string;
    }).join('&');
  }


  private async _fetchData()
  {
    
    if(!this._underlyingMap){
      return;
    }


    

    const mapBounds = this._underlyingMap!.getBounds()!;

    const lower = {lat: mapBounds.getNorthEast().toJSON().lat, lon: mapBounds.getNorthEast().toJSON().lng};
    const upper = {lat: mapBounds.getSouthWest().toJSON().lat, lon: mapBounds.getSouthWest().toJSON().lng};

    const paramBounds = {low: lower, up: upper};

    const queryPart = this._buildHttpQueryPart(paramBounds);
    
    const response = await fetch(`${host}/fetch?${queryPart}`, {
       method: 'POST',
       body: JSON.stringify(paramBounds),       
       cache: "no-cache",
       headers: {
         "content-type": "application/json"
       }       
     });


     try{
      const data = await response.json() as {
        lng: number,
        lat: number,
        cnt: number        	
      }[];
    



      const mapPoints = [...data.map(x => {

        const point = new google.maps.LatLng(x.lat, x.lng);

        return {location: point, weight: x.cnt} as WeightedLocation;
      })];

      this._heatmapLayer?.setData(mapPoints)
    }catch(e){
      console.log('ERROR: ', e, response);
    }
  }
  
   
  public componentDidMount(){
    if('geolocation' in navigator){
      navigator.geolocation.getCurrentPosition((pos) => this._updatePosition(pos))
      navigator.geolocation.watchPosition((pos) => this._updatePosition(pos))
    }
  }
 
  async _updatePosition(pos: Position) {

    this._lastPos = pos;

    await this._throttle(() => this._lastPos!, 2000)
      .then((pos) => this._sendPosToServer(pos));
  }


  async _sendPosToServer(pos: Position) {
      const payload = {
        loc: {
          lon: this._lastPos?.coords.longitude,
          lat: this._lastPos?.coords.latitude,
        },
        count: 1
    };


    

    let response: Response | null = null;
    try{
      response = await fetch(`${host}` + (this._lastPositionId ? `/${this._lastPositionId}` :  ""), 
      {
        method: "POST",
        body: JSON.stringify(payload),
        headers: {
          'content-type': 'application/json'
        },
        cache: "no-cache",
      });

      //this._lastPositionId = await response.json() as string;
    }
    catch(e){
      console.log('ERROR: ', e, response);
    }

    console.log(`_sendPosToServer (response): ${this._lastPositionId}`);
  }
  
  
  _throttle(valPointer: () => Position, millis : number) : Promise<Position>
  {
    return new Promise((res) => {

      const entryVal = valPointer();

      setTimeout(() => {
        if(entryVal === valPointer()){
          res(entryVal);
        }
      }, millis)
    });
  }


  public render(){

    const defaultCenter = toJS(this.mapCenter);

    return (<div className="App">
              <header className="App-header">
               
              <GoogleMapReact
                //layerTypes={["satellite"]}
                //ref={(m) => this._googleMap = m}
                style={{width: '100%', height: window.window.innerHeight}}
                bootstrapURLKeys={{ key: 'AIzaSyDf7xOAWnBblHu6giZxki8kE2vrCx9rYho' }}
                defaultCenter={defaultCenter}
                center={this.mapCenter}
                defaultZoom={this.mapZoom}
                yesIWantToUseGoogleMapApiInternals
                heatmapLibrary={true}        
                onGoogleApiLoaded={({ map, maps, ref}) => this.handleApiLoaded(map as google.maps.Map<HTMLDivElement>, maps, ref)}  
                //heatmap={heatMapData}  
              >
              </GoogleMapReact>
              </header>
          </div>);
  }
}